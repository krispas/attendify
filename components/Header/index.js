import React from 'react'
import { observer } from 'startupjs'
import { Div, Span } from '@startupjs/ui'
import './index.styl'

export default observer(function Header () {
  return pug`
    Div.root
      Span.title Transparent pricing, unrivaled value
  `
})
