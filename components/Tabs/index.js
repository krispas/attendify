import React, { useState } from 'react'
import { Dimensions } from 'react-native'
import { observer } from 'startupjs'
import { Div, Menu, Span } from '@startupjs/ui'
import { BASE_URL } from 'clientHelpers'
import './index.styl'

const MenuItem = Menu.Item
const MENU_ITEMS = [
  'VIRTUAL+APP',
  'REGISTRATION',
  'PACKAGES',
]

export default observer(function Tabs () {
  const [active, setActive] = useState(0)

  return pug`
    Div.root
      Div.menu
        each item, i in MENU_ITEMS
          Div.item(
            styleName={first: !i, active: active === i} 
            onPress=() => setActive(i)
            key=item
          )
            Span.itemText(styleName={active: active === i})=item
  `
})
