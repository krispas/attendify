import React, { useEffect } from 'react'
import { observer, emit, useSession, $root } from 'startupjs'
import { Icon, Div } from 'components'
// import MessageInput from 'main/components/MessageInput'
// import CreatePostInput from 'main/components/CreatePostInput'
// import JoinButtons from '../JoinButtons'
// import SubscribePage from '../SubscribePage'
// import SwitchSchedule from 'main/components/SwitchSchedule'
import colors from 'styles/colors'
import './index.styl'

export default observer(function Footer () {
  const [footer = { component: 'hide' }, $footer] = useSession('footer')

  const defaultFooterData = [
    'home',
    'registration',
    'addEvent',
    'webView',
    'community'
  ]

  useEffect(() => {
    let redirectUrl
    switch (footer.component) {
      case 'home':
        redirectUrl = '/'
        break
      case 'addEvent':
        redirectUrl = '/?myevent=true'
        break
      case 'registration':
        redirectUrl = '/registration'
        break
      case 'webView':
        redirectUrl = '/web-view'
        break
      case 'community':
        redirectUrl = '/audience'
        break
      default:
        return
    }
    emit('url', redirectUrl)
  }, [footer.component])

  const renderComponent = () => {
    const { component, props } = footer

    // const items = {
    //   ['MessageInput']: MessageInput,
    //   ['CreatePostInput']: CreatePostInput,
    //   ['JoinButton']: JoinButtons,
    //   ['SubscribePage']: SubscribePage,
    //   ['SwitchSchedule']: SwitchSchedule
    // }

    const Component = items[component]
    return pug`
      Component.wide(...props showAvatar styleName={joinButton: component === 'JoinButton'})
    `
  }

  if (footer.component === 'hide') return null

  return pug`
    Div.root(styleName={input: !defaultFooterData.includes(footer.component)})
      if defaultFooterData.includes(footer.component)
        Div.item(activeOpacity=0.8 onPress= () => $footer.set('component', 'home'))
          Icon(name='home' type='fas' size='m' color= (footer.component === 'home') ? colors.primary : colors.gray)
        Div.item(activeOpacity=0.8 onPress= () => $footer.set('component', 'registration'))
          Icon(name='ticket-alt' type='fas' size='m' color= (footer.component === 'registration') ? colors.primary : colors.gray)
        Div.item(activeOpacity=0.8 onPress= () => $footer.set('component', 'addEvent'))
          Icon(name='plus-square' type='far' size='m' color= (footer.component === 'addEvent') ? colors.primary : colors.gray)
        Div.item(activeOpacity=0.8 onPress= () => $footer.set('component', 'webView'))
          Icon(name='desktop' type='fas' size='m' color= (footer.component === 'webView') ? colors.primary : colors.gray)
        Div.item(activeOpacity=0.8 onPress= () => $footer.set('component', 'community'))
          Icon(name='user-friends' type='fas' size='m' color= (footer.component === 'community') ? colors.primary : colors.gray)
      else
        = renderComponent()
  `
})

export function switchFooter (component, props = {}) {
  const $footer = $root.scope('_session.footer')
  $footer.setEach({ component, props })
}

export function hideFooter () {
  const $footer = $root.scope('_session.footer')
  $footer.set({ component: 'hide' })
}
