import React, { useState } from 'react'
import { Image } from 'react-native'
import { observer } from 'startupjs'
import { OverlineText } from 'components'
import { Div, Span, H4, Button, Rating } from '@startupjs/ui'
import { BASE_URL } from 'clientHelpers'
import './index.styl'

const RATING_ITEMS = [
  { icon: '0.png', rate: 4.6, style: {width: 104, height: 25} },
  { icon: '1.png', rate: 4.7, style: {width: 85, height: 26} },
  { icon: '2.png', rate: 4.8, style: {width: 100, height: 25} },
  { icon: '3.png', rate: 4.6, style: {width: 114, height: 29} }
]

export default observer(function BlueBlock () {

  return pug`
    Div.root
      Div.title
        H4.titleText(bold) "Communication at our event is huge. We used the Attendify event app’s features to surprise and delight our attendees."
      Div.author
        Image.photo(source={uri: BASE_URL + '/photo.png'})
        Span.authorText Amanda Reynolds @ Balfour's
        Button.button(onPress=() => {})
          Span.buttonText READ CASE STUDY
      Div.ratingWrapper
        each rating, i in RATING_ITEMS
          Div.rating(key=rating.icon styleName={first: !i})
            Image.icon(source={uri: BASE_URL + '/rating/' + rating.icon} style=rating.style)
            Div.stars
              Rating(value=rating.rate size='xs')
              Span.starText=rating.rate + '/5'
  `
})
