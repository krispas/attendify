import React, { useState } from 'react'
import { observer } from 'startupjs'
import { OverlineText } from 'components'
import { Div, Menu, Span, Radio } from '@startupjs/ui'
import './index.styl'

export default observer(function Options ({ title, data }) {
  const [selectedItem, setSelectedItem] = useState(data[0] && data[0].text)

  return pug`
    Div.root
      if title
        Span.title(bold)=title
      Div.options
        each option, i in data
          Div.option(key=option.text styleName={first: !i})
            Div.left
              Radio(
                color='#268bff' 
                data=[{value: option.text, label: option.text}] 
                value=selectedItem
                onChange=setSelectedItem
              )
            Div.right
              if option.included
                Span.blueText Included
              if option.price
                Span.price(bold)=option.price
              if option.oldPrice
                OverlineText
                  Span.oldPrice=option.oldPrice
  `
})
