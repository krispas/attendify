import React from 'react'
import { observer } from 'startupjs'
import { Div, Span, H5, Icon } from '@startupjs/ui'
import DATA from './data.json'
import Item from './Item'
import './index.styl'

export default observer(function QAList () {
  return pug`
    Div.root
      Div.categoryList
        each category, i in DATA
          Div.item(styleName={first: !i} key=category.category)
            H5=category.category
            Div.itemList
              each item in category.data
                Item(...item key=item.question)
  `
})
