import React, {useState} from 'react'
import { observer } from 'startupjs'
import { Div, Span,Icon, Collapse } from '@startupjs/ui'
import { faCaretDown } from '@fortawesome/free-solid-svg-icons'
import './index.styl'

const CollapseHeader = Collapse.Header
const CollapseContent = Collapse.Content

export default observer(function Item ({ answer, question, list }) {
  const [open, setOpen] = useState(false)
  return pug`
    Div.root
      Collapse(open=open onChange=() => setOpen(!open))
        CollapseHeader
          Span.question=question
        CollapseContent
          Div.content
            Span.answer=answer
            if list
              Div.list
                each item, i in list
                  Div.listItem(styleName={first: !i} key=item)
                    Span.listItemText #{i + 1}. #{item}
  `
})
