import React, { useState } from 'react'
import { Image } from 'react-native'
import { observer } from 'startupjs'
import { BASE_URL } from 'clientHelpers'
import { Div, Span, H4, Button, H5 } from '@startupjs/ui'
import './index.styl'

export default observer(function PromoBlock () {
  return pug`
    Div.root
      Image.img(source={uri: BASE_URL + '/pc.png'})
      Div.container
        H5(bold) Give us a try before you buy
        Span.text Take Attendify for a spin. No software to install. No timelines. No credit card required until decide to publish.
        Button.button GET STARTED FREE
  `
})
