import React from 'react'
import { observer } from 'startupjs'
import { Div, Span, Icon } from '@startupjs/ui'
import { faCircle } from '@fortawesome/free-solid-svg-icons'
import './index.styl'

const LIST_DATA = [
  'Immersive virtual event',
  'Integrated streaming',
  'Top rated mobile event app',
  'Attendee networking',
  'Channels & group chats',
  'Live polling',
  'Push notifications',
  'Turnkey configuration',
  'Simple, accessible management',
  'Real-time analytics'
]

export default observer(function CheckList () {
  return pug`
    Div.root
      Span.title Included:
      Div.list
        each item, i in LIST_DATA
          Div.item(styleName={first: !i} key=item)
            Icon(icon=faCircle size=5)
            Span.itemText=item
  `
})
