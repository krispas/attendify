import React from 'react'
import { Image } from 'react-native'
import { observer } from 'startupjs'
import { Div, Span } from '@startupjs/ui'
import { BASE_URL } from 'clientHelpers'
import './index.styl'

const LOGOS = [
  {style: {width: 141, height: 29}, uri: '0.png'},
  {style: {width: 142, height: 40}, uri: '1.png'},
  {style: {width: 68, height: 55}, uri: '2.png'},
  {style: {width: 53, height: 29}, uri: '3.png'},
  {style: {width: 113, height: 32}, uri: '4.png'},
  {style: {width: 123, height: 27}, uri: '5.png'},
  {style: {width: 112, height: 32}, uri: '6.png'},
  {style: {width: 135, height: 24}, uri: '7.png'},
  {style: {width: 132, height: 36}, uri: '8.png'},
  {style: {width: 77, height: 44}, uri: '9.png'},
  {style: {width: 132, height: 23}, uri: '10.png'}
]

export default observer(function PartnerLogos () {
  return pug`
    Div.root
      each logo, i in LOGOS
        Image.logo(
          source={uri: BASE_URL + '/partners/' + logo.uri} 
          style=logo.style 
          key=logo.uri
          styleName={first: !i}
        )
  `
})
