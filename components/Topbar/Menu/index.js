import React, { useState } from 'react'
import { observer } from 'startupjs'
import { Div, Menu, Span } from '@startupjs/ui'
import { BASE_URL } from 'clientHelpers'
import './index.styl'

const MenuItem = Menu.Item
const MENU_ITEMS = [
  'Products',
  'How it Works',
  'Pricing',
  'Customers',
  'Company'
]

export default observer(function TopbarMenu () {
  const [active, setActive] = useState('option-1')

  return pug`
    Menu(variant='horizontal')
      each item, i in MENU_ITEMS
        MenuItem.item(styleName={first: !i} style={color:'#fff'} onPress=() => {} key=item)
          Span.itemText=item
  `
})
