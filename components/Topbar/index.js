import React from 'react'
import { observer } from 'startupjs'
import { Image } from 'react-native'
import { Content, Div, H4, H3, Button, Icon } from '@startupjs/ui'
import Menu from './Menu'
import { faBars } from '@fortawesome/free-solid-svg-icons'
import { BASE_URL } from 'clientHelpers'
import './index.styl'

export default observer(function Topbar () {
  return pug`
    Div.root
      Div.mobile
      Div.logo
        H3.logoText attendify
      Div.menu
        Menu
      Div.getStarted
        Div.desktop
          Button.button GET STARTED - IT'S FREE
        Div.mobile
          Icon(icon=faBars size='l' color='#fff')
  `
})
