import React from 'react'
import { observer } from 'startupjs'
import { Div, Span } from '@startupjs/ui'
import { OverlineText } from 'components'
import './index.styl'

const data = {
  title: 'Engagement on any screen (virtual + app)',
  text: `Whether you're running an in-person, virtual or hybrid event, Attendify delivers an engaging experience on every device in a turn-key package that's easy to deploy and manage.`,
  price: '$699',
  oldPrice: '$999'
}

export default observer(function SingleItem () {
  return pug`
    Div.root
      Div.textBlock
        Span.title(bold)=data.title
        Span.text=data.text
      Div.priceBlock
        Div.oldPrice
          OverlineText
            Span=data.oldPrice
        Span.price=data.price
  `
})
