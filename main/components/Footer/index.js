import React from 'react'
import { observer } from 'startupjs'
import Footer from 'components/Footer'

export default observer(function () {
  return pug`
    Footer
  `
})
