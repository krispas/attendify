import React from 'react'
import {
  View,
  Text,
  TouchableOpacity,
  Platform,
  Dimensions
} from 'react-native'
import { observer, emit, useOn, useValue, useSession } from 'startupjs'
import Footer from 'components/Footer'
import moment from 'moment'
import './index.styl'

export default observer(function () {
  const [url, $url] = useSession('url')

  const [hideFooter, $hideFooter] = useValue(false)
  const redirect = url => {
    emit('url', url)
    emit('SecondTopbar.hide')
  }

  useOn('Footer.hide', () => $hideFooter.set(true))
  useOn('Footer.show', () => $hideFooter.set(false))

  const isWeb = Platform.OS === 'web' && Dimensions.get('window').width > 480

  return pug`
    unless hideFooter
      View
      if isWeb
        View.root
          View.content
            TouchableOpacity.menuItem(onPress= () => redirect('/contact-us'))
              Text.tab='Contact Us'
            TouchableOpacity.menuItem(onPress= () => redirect('/terms-of-service'))
              Text.tab='Terms of Service'
            TouchableOpacity.menuItem(onPress= () => redirect('/privacy-policy'))
              Text.tab='Privacy Policy'
            View
              Text.tab='\u00A9 WeGroup ' +  moment().format('YYYY')
      else if !/\/join-event\/.*/.test(url)
        Footer
  `
})
