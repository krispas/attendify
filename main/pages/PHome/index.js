import React from 'react'
import { observer } from 'startupjs'
import { ScrollView, ImageBackground } from 'react-native'
import {
  Header,
  Tabs,
  SingleItem,
  CheckList,
  Options,
  PartnerLogos,
  BlueBlock,
  QAList,
  PromoBlock,
  Topbar
} from 'components'
import { Footer } from 'main/components'
import { Div, H4, Span } from '@startupjs/ui'
import { BASE_URL } from 'clientHelpers'
import './index.styl'

const OPTION_DATA = [
  {
    text: 'Attendify Event App',
    included: true,
    popup: ''
  },
  // {
  //   text: 'Branded Event App',
  //   price: '+ $700',
  //   oldPrice: '+$1,000'
  // }
]

const OPTION_DATA2 = [
  {
    text: 'Integrate YouTube, Vimeo or Zoom',
    included: true,
    popup: ''
  },
  {
    text: 'Attendify S',
    price: '+ $700',
    oldPrice: '+$1,000'
  },
  {
    text: 'Attendify M',
    price: '+ $1,225',
    oldPrice: '+$1,750'
  },
  {
    text: 'Attendify L',
    price: '+ $1,750',
    oldPrice: '+$2,500'
  },
  {
    text: 'Attendify XL',
    price: '+ $2,800',
    oldPrice: '+$4,000'
  },
]

export default observer(function PHome () {
  return pug`
    ScrollView.root
      Div.container
        ImageBackground.bg(source={uri: BASE_URL + '/images/bgHeader.jpg'})
          Topbar
          Header
          Tabs
        Div.content
          SingleItem
          Div.responsiveContainer
            CheckList
            Div.options
              Span.optionsText Options:
              Options(title='Mobile event app' data=OPTION_DATA)
              Options(title='Streaming' data=OPTION_DATA2)
        Div.wrapper
          Div.wrapperBlock
            PartnerLogos
        Div.wrapper(styleName={blue: true})
          Div.wrapperBlock
            BlueBlock
        Div.wrapper
          Div.wrapperBlock
            H4.title(bold) Frequently asked questions:
            QAList
        Div.wrapper
          Div.wrapperBlock
            PromoBlock
        Div.wrapper(styleName={gray: true})
          Div.wrapperBlock
        Footer
  `
})
